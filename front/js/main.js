// Open-close FAQ
let details = document.querySelectorAll("details");
for(let i = 0; i < details.length; i++) {
    details[i].addEventListener("toggle", accordion);
}
function accordion(event) {
    if (!event.target.open) return;
    let details = event.target.parentNode.children;
    for(let i = 0; i < details.length; i++) {
        if (details[i].tagName !== "DETAILS" ||
            !details[i].hasAttribute('open') ||
            event.target === details[i]) {
            continue;
        }
        details[i].removeAttribute("open");
    }
}

let articlesAll = document.querySelector('.articles__all')
let articlesMore = document.querySelector('.articles__more')

articlesMore.addEventListener('click', () => {
    articlesAll.classList.toggle('toggle-list');
    articlesMore.style.display = "none";
});

let searchBtn = document.querySelector('.articles__filter-search .search-btn')
let providersSearch = document.querySelector('.articles__filter-search .articles-search')
let closeBtn = document.querySelector('.articles__filter-search .close-btn')
let submitBtn = document.querySelector('.articles__filter-search .btn-submit')

searchBtn.addEventListener('click', () => {
    searchBtn.style.display = "none";
    providersSearch.style.display = "block";
    closeBtn.style.display = "block";
    submitBtn.style.display = "none";
});

closeBtn.addEventListener('click', () => {
    searchBtn.style.display = "flex";
    providersSearch.style.display = "none";
    closeBtn.style.display = "none";
});

let seoInfo = document.querySelector('.seo-text__info')
let seoBtn = document.querySelector('.seo-text__more')

seoBtn.addEventListener('click', () => {
    seoInfo.classList.toggle('toggle-list');
    seoBtn.classList.toggle('toggle-arrow');
});